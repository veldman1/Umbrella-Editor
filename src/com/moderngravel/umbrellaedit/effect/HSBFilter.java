package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;
import java.util.Map;
import java.util.TreeMap;

import com.moderngravel.umbrellaedit.framework.Filter;
import com.moderngravel.umbrellaedit.util.ColorUtil;

public class HSBFilter extends Filter {

	float h;
	float s;
	float b;

	public void init(Map<String, Object> params) {
		this.h = (Integer.valueOf(params.get("h").toString())) / 100f;
		this.s = (Integer.valueOf(params.get("s").toString())) / 100f;

		this.b = (Integer.valueOf(params.get("b").toString())) / 100f;

	}

	@Override
	public String getName() {
		return "Hue/Saturation/Brightness";
	}

	@Override
	public Map<String, String> getParams() {
		Map<String, String> params = new TreeMap<String, String>();
		params.put("b", "percent");
		params.put("s", "percent");

		params.put("h", "percent");

		return params;
	}

	@Override
	public Color filterPixel(Color col, int x, int y) {
		float hue = ColorUtil.getHue(col);
		float saturation = ColorUtil.getSaturation(col);
		float brightness = ColorUtil.getBrightness(col);

		hue = hue + .5f;
		hue += h;
		hue = hue % 1;
		if (x == 1 && y == 0) {
			System.out.println(brightness);
		}
		
		saturation *= s * 2;
		
		if (saturation > 1){
			saturation = 1;
		}
		
		brightness *= b * 2;
		
		if (brightness > 1){
			brightness = 1;
		}


		return Color.getHSBColor(hue, saturation, brightness);
	}
}
