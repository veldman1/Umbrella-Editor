package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;

import com.moderngravel.umbrellaedit.framework.Effect;

public class SobelEffect implements Effect {
	private int[][] weights = { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };

	private int max = 0;

	private int width, height;
	private BufferedImage input;

	@Override
	public String getName() {
		return "Sobel";
	}

	@Override
	public BufferedImage getProcessedImage(BufferedImage currentImage, JFrame frame) {
		input = currentImage;
		width = currentImage.getWidth();
		height = currentImage.getHeight();
		BufferedImage newImage = new BufferedImage(width, height, currentImage.getType());
		for (int x = 1; x < width; x++) {
			for (int y = 1; y < height; y++) {
				newImage.setRGB(x, y, filterPixel(new Color(currentImage.getRGB(x, y)), x, y).getRGB());
			}
		}
		return newImage;
	}

	public Color filterPixel(Color col, int x, int y) {
		int sumX = 0;
		int sumY = 0;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				int xLoc = x + i;
				if (xLoc < 0 || xLoc >= width) {
					xLoc = x;
				}
				int yLoc = y + j;
				if (yLoc < 0 || yLoc >= height) {
					yLoc = y;
				}
				Color colAt = new Color(input.getRGB(xLoc, yLoc));
				int edgeX = weights[i + 1][j + 1];
				int edgeY = weights[j + 1][i + 1];

				int avg = (colAt.getRed() + colAt.getGreen() + colAt.getBlue()) / 3;
				sumX += edgeX * avg;
				sumY += edgeY * avg;
			}
		}

		double distance = Math.sqrt(Math.pow(sumX, 2) + Math.pow(sumY, 2));
		if (distance > max) {
			max = (int) distance;
		}
		int value = (int) ((distance * 265) / 1550);
		return new Color(value, value, value);
	}

	public int getMax() {
		return max;
	}

	@Override
	public void init(Map<String, Object> params) {

	}

	@Override
	public Map<String, String> getParams() {
		return new TreeMap<String, String>();
	}
}
