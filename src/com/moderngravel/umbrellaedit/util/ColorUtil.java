package com.moderngravel.umbrellaedit.util;

import java.awt.Color;

public class ColorUtil {

	public static float getHue(Color col) {
		float r = col.getRed();
		float g = col.getGreen();
		float b = col.getBlue();

		float min = Math.min(Math.min(r, g), b);
		float max = Math.max(Math.max(r, g), b);

		float h = 0f;

		if (max == r) {
			h = (g - b) / (max - min);

		} else if (max == g) {
			h = 2f + (b - r) / (max - min);

		} else {
			h = 4f + (r - g) / (max - min);
		}
		h = h * (256/6);
		if (h < 0) {
			h = h + 256;
		}

		return h / 256;
	}

	public static float getSaturation(Color col) {
		float r = col.getRed();
		float g = col.getGreen();
		float b = col.getBlue();

		float min = Math.min(Math.min(r, g), b);
		float max = Math.max(Math.max(r, g), b);

		float s;
		if (r == g && g == b) {
			s = 0;
		} else {
			s = max - min;
			s = s /255;
		}
		return s;
	}

	public static float getBrightness(Color col) {
		return (col.getRed()/3f+col.getBlue()/3f+col.getRed()/3f)/256f;
		/*return (0.2126f * col.getRed() + 0.7152f * col.getGreen() + 0.0722f * col
				.getBlue()) / 256;*/
	}

}
