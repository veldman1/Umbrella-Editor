package com.moderngravel.umbrellaedit.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Splash {
	public Splash() {

		try {
			if (System.getProperty("os.name").equals("Windows 10")) {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} else {
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if ("Nimbus".equals(info.getName())) {
						UIManager.setLookAndFeel(info.getClassName());
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Theme not found");
		}

		JFrame splash = new JFrame("");
		JMenuBar mb = new JMenuBar();
		splash.setTitle("umbrella");

		ImageIcon umbrellaIcon = new ImageIcon(getClass().getClassLoader().getResource("./umbrella.png"));
		splash.setIconImage(umbrellaIcon.getImage());

		JMenu menu = new JMenu("File");

		JMenuItem menuItem = new JMenuItem("Open...");

		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					JFileChooser openFileChooser = new JFileChooser();
					File pictures = new File(System.getProperty("user.home") + "/Pictures");
					if (pictures.isDirectory()) {
						openFileChooser.setCurrentDirectory(pictures);
					}
					if (openFileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
						return;
					}
					File file = openFileChooser.getSelectedFile();
					new Window(file);
					splash.dispose();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		menu.add(menuItem);

		mb.add(menu);

		splash.setJMenuBar(mb);

		JPanel jp = new JPanel(new BorderLayout());
		jp.add(new JLabel(umbrellaIcon));
		splash.add(jp);
		splash.setSize(500, 500);
		splash.setResizable(false);
		splash.setVisible(true);
		splash.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		splash.pack();
		splash.setLocationRelativeTo(null);
	}

	public static void main(String args[]) {
		new Splash();
	}
}
