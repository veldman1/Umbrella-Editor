package com.moderngravel.umbrellaedit.effect;
import java.awt.Color;

import com.moderngravel.umbrellaedit.framework.Filter;

public class InvertFilter extends Filter {

	@Override
	public String getName(){
		return "Invert";
	}
	
	@Override
	public Color filterPixel(Color col, int x, int y){
		Color newCol = new Color(255-col.getRed(),255-col.getGreen(),255-col.getBlue());
		return newCol;
	}

	

}
