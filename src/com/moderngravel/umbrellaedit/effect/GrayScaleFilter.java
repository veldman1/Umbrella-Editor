package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;

import com.moderngravel.umbrellaedit.framework.Filter;

public class GrayScaleFilter extends Filter {

	@Override
	public String getName() {
		return "Grayscale Filter";
	}

	@Override
	public Color filterPixel(Color col, int x, int y){
		int avg = (col.getRed() + col.getBlue() + col.getGreen()) / 3;
		Color newCol = new Color(avg, avg, avg);
		return newCol;
	}

}
