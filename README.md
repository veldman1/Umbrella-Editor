
# Umbrella-Editor
Fun filters and effects, all under one umbrella! My own bootleg photoshop.

The seam-carving algorithm is an implementation of [this](http://www.faculty.idc.ac.il/arik/site/seam-carve.asp) SIGGRAPH paper.