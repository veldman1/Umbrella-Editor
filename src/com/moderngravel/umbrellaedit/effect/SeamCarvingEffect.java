package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;

import com.moderngravel.umbrellaedit.framework.Effect;
import com.moderngravel.umbrellaedit.util.DijkstraVertex;

public class SeamCarvingEffect implements Effect {
	private int numremove;
	private boolean mark;
	private JFrame frame;

	@Override
	public BufferedImage getProcessedImage(BufferedImage currentImage, JFrame frame) {
		for (int i = 0; i < numremove; i++) {
			frame.setTitle((i + 1) + "/" + numremove);
			currentImage = removeOneSeam(currentImage);
		}
		return currentImage;
	}

	public BufferedImage removeOneSeam(BufferedImage currentImage) {
		SobelEffect sb = new SobelEffect();
		BufferedImage sbImage = sb.getProcessedImage(currentImage, null);
		int width = currentImage.getWidth();
		int height = currentImage.getHeight();

		int[][] intensityArray = new int[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color color = new Color(sbImage.getRGB(i, j));
				intensityArray[i][j] = (color.getGreen() + color.getRed() + color
						.getBlue()) / 3;
			}
		}

		DijkstraVertex[][] backtrace = new DijkstraVertex[width][height];
		List<DijkstraVertex> bottomRow = new ArrayList<DijkstraVertex>();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width - 1; x++) {
				if (y == 0) {
					backtrace[x][y] = new DijkstraVertex(x, y,
							intensityArray[x][y]);
				} else {
					DijkstraVertex center = backtrace[x][y - 1];
					DijkstraVertex left = null, right = null;
					int locXL = x-1;
					if (x - 1 < 0) {
						locXL = 0;
					}
					int locXR = x+1;
					if (x + 1 > width - 2) {
						locXR = width - 2;
					}
					left = backtrace[locXL][y - 1];
					right = backtrace[locXR][y - 1];

					DijkstraVertex back = center;

					if (right.getValue() < center.getValue()) {
						back = right;
					}

					if (left.getValue() < center.getValue()
							&& left.getValue() < right.getValue()) {
						back = left;
					}

					if (x - 1 < 0) {
						back = right;
					}
					if (x + 1 > width - 2) {
						back = left;
					}

					int bestIntensity = back.getValue();

					DijkstraVertex newDV = new DijkstraVertex(x, y,
							bestIntensity + intensityArray[x][y]);
					newDV.setLast(back);
					backtrace[x][y] = newDV;
					if (y == height - 1 && (x > width / 6)
							&& x < (width - width / 6)) {
						bottomRow.add(newDV);
					}
				}
			}
		}
		BufferedImage newImage = null;

		int bestPathLoc = Integer.MAX_VALUE;
		int bestLocation = width / 2;
		DijkstraVertex bestDijkstraVertex = null;
		for (DijkstraVertex dijkstraVertex : bottomRow) {
			int thisBest = dijkstraVertex.getValue();
			if (thisBest < bestPathLoc) {
				bestDijkstraVertex = dijkstraVertex;
				bestLocation = dijkstraVertex.getX();
				bestPathLoc = thisBest;

			}

		}
		bottomRow.remove(bestDijkstraVertex);

		List<DijkstraVertex> bestPath = new ArrayList<DijkstraVertex>();
		DijkstraVertex tracePoint = backtrace[bestLocation][height - 1];
		while (tracePoint != null) {
			bestPath.add(tracePoint);
			int move = 0;
			if (mark) {
				move = 1;
			}
			currentImage.setRGB(tracePoint.getX() + move, tracePoint.getY(),
					0xFF0000);
			tracePoint = tracePoint.getLast();
		}

		newImage = new BufferedImage(currentImage.getWidth() - 1, height,
				currentImage.getType());

		for (int y = 0; y < height - 1; y++) {
			int purge = bestPath.get(height - y - 1).getX();
			int offset = -1;
			for (int x = 0; x < currentImage.getWidth() - 1; x++) {
				offset++;
				if (purge == x) {
					offset--;
				} else {
					newImage.setRGB(offset, y, currentImage.getRGB(x, y));
				}
			}
		}

		currentImage = newImage;

		return currentImage;
	}

	@Override
	public String getName() {
		return "Seam Carving";
	}

	@Override
	public void init(Map<String, Object> params) {
		numremove = Integer.valueOf(params.get("reduce width by").toString());
		mark = Boolean.valueOf(params.get("mark").toString());
	}

	@Override
	public Map<String, String> getParams() {
		Map<String, String> map = new TreeMap<String, String>();
		map.put("reduce width by", "percent");
		map.put("mark", "boolean");
		return map;
	}

}
