package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import com.moderngravel.umbrellaedit.framework.Filter;

public class BellCurveBlurFilter extends Filter {

	private int size;
	double boxSize;
	double[][] weights;

	public void init(Map<String,Object> params) {
		
		this.size = Integer.valueOf((params.get("size").toString()));
		if (size < 5) {
			size = 5;
		}
		size = size / 5;
		int across = size*2+1;
		weights = new double[across][across];
		
		for (int i = 0; i < across; i++) {
			for (int j = 0; j < across; j++) {
				double distance = Math.sqrt(Math.pow(i-size, 2)+Math.pow(j-size, 2));
				weights[i][j] = bellCurve((distance/(size*2)));
				//weights[i][j] =1;
			}
		}
		
		boxSize = 0;
		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights.length; j++) {
				boxSize += weights[i][j];
			}
		}
		
	}
	
	@Override
	public String getName(){
		return "Bell-Curve Blur";
	}

	private double bellCurve(double x) {
		double answer = (Math.sin(2 * Math.PI * ((x+.5)/2)) + 1)*2;
		return answer;
	}

	@Override
	public Color filterPixel(Color col, int x, int y){
		int sumR = 0;
		int sumG = 0;
		int sumB = 0;
		for (int i = -size; i <= size; i++) {
			for (int j = -size; j <= size; j++) {
				int xLoc = x + i;
				if (xLoc < 0 || xLoc >= width) {
					xLoc = x;
				}
				int yLoc = y + j;
				if (yLoc < 0 || yLoc >= height) {
					yLoc = y;
				}
				Color colAt = new Color(input.getRGB(xLoc, yLoc));

				double mult = weights[i+size][j+size];
				
				sumR += colAt.getRed()*mult;
				sumG += colAt.getGreen()*mult;
				sumB += colAt.getBlue()*mult;
			}
		}
		int r = (int) (sumR / boxSize);
		int g = (int) (sumG / boxSize);
		int b = (int) (sumB / boxSize);
		return new Color(r, g , b);
	}
	
	@Override
	public Map<String, String> getParams(){
		Map<String, String> params = new HashMap<String,String>();
		params.put("size", "percent");
		return params;
	}

}
