package com.moderngravel.umbrellaedit.framework;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import javax.swing.JFrame;

import com.moderngravel.umbrellaedit.util.ComputeAction;

public abstract class Filter implements Effect {
	
	protected int height;
	protected int width;
	protected BufferedImage input;
	
	public BufferedImage getProcessedImage(BufferedImage currentImage, JFrame frame) {
		this.input= currentImage;
		width = currentImage.getWidth();
		height = currentImage.getHeight();
		ComputeAction compute = new ComputeAction(currentImage,0,height,this,width, false);
		ForkJoinPool fp = new ForkJoinPool();
		fp.invoke(compute);
		return compute.getResult();
	}

	public abstract Color filterPixel(Color col, int x, int y);

	public abstract String getName();

	public void init(Map<String, Object> params){
		
	};

	public Map<String, String> getParams() {
		return new HashMap<String, String>();
	}
}